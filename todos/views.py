from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list_list": todos}
    return render(request, "todos/list.html", context)


def todo_item(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {"todo_items": item}
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoForm()
    context = {"todo_list_form": form}
    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    edits = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=edits)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=edits)
    context = {"todo_list": edits, "update_form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            todo_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = ItemForm()

    context = {
        "item_form": form,
    }

    return render(request, "todos/create_item.html", context)


def edit_todo_item(request, id):
    edits = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=edits)
        if form.is_valid():
            form.save()
            todo_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = ItemForm(instance=edits)

    context = {"item_form": form}

    return render(request, "todos/edit_item.html", context)
